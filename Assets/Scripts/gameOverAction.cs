using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class gameOverAction : MonoBehaviour
{
	public Text high_score;
  	private bool _AlreadyClicked = false;
    // Start is called before the first frame update
    void Start()
    {
    	int highScore =  PlayerPrefs.GetInt("HighScore",0);
    	int score = GameState.Instance.getScorePlayer();
    	if( score > highScore){
       		PlayerPrefs.SetInt("HighScore",score);
       		high_score.text = " New HighScore : "+score.ToString();
       	}
       	else
       		high_score.text = "HighScore :"+highScore.ToString();
    }

    // Update is called once per frame

    
    
    void Update()
    { 
        if(Input.anyKeyDown) { 
                        // Debug.Log(_AlreadyClicked);

            if (_AlreadyClicked){
                _AlreadyClicked = false;
     
                SoundState.Instance.makeclickSound();
                SoundState.Instance.stopMusic();
                SceneManager.LoadScene("scene2-Menu");
            }
            else  _AlreadyClicked = true;  
         }
    }
}
