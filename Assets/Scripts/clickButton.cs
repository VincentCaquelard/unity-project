﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class clickButton : MonoBehaviour
{
    public void onClick()
    {
        GameState.Instance.resetScore();
        SoundState.Instance.startMusic();
        SoundState.Instance.makeclickSound();
       
        SceneManager.LoadScene("scene3-Game_1");
    
    }
}
