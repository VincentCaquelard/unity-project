﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CameraDatas;

public class loopBackground : MonoBehaviour
{
	  private Vector2 movement;
	  private float speed = -2;
      private Vector2 size;
      public float positionRestartX;
    // Start is called before the first frame update
    void Start()
    {
        size = gameObject.GetComponent<SpriteRenderer>().bounds.size;
        positionRestartX= GameObject.Find("backGround3").GetComponent<Transform>().position.x;

    }

    // Update is called once per frame
    void Update()
    {
        movement = new Vector2( speed , 0);

       gameObject.GetComponent<Rigidbody2D>().velocity = movement;

       if(  gameObject.GetComponent<Transform>().position.x  < datas.cameraBorderDotLeftBottom.x-(size.x/2)){
            gameObject.GetComponent<Transform>().position = new Vector2(positionRestartX,0);
       }


    }
}