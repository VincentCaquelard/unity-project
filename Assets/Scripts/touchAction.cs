﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CameraDatas;


public class touchAction : MonoBehaviour
{
	public float speedUp;

    private Vector2 birdSize;
    private Vector3 birdOldPosition;
    private Vector2 birdNewPosition;
    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0,speedUp);
        birdSize =  gameObject.GetComponent<SpriteRenderer>().bounds.size;
    }

    // Update is called once per frame
    void Update()
    {
         birdOldPosition = gameObject.GetComponent<Transform>().position;  // Récupération de la position de l'objet.

        birdNewPosition = birdOldPosition;  // Valeur par défaut (cas où l'objet est dans le champs de vision de la caméra).

        if (birdOldPosition.y + (birdSize.y/3) > datas.cameraBorderDotLeftTop.y)
          birdNewPosition.y = datas.cameraBorderDotLeftTop.y - (birdSize.y/3);        // L'objet veut sortir en haut

         if (Input.anyKey)
        {
             SoundState.Instance.makeflapSound();
        	gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0,speedUp);
        
        }
        gameObject.GetComponent<Transform>().position = birdNewPosition;
    }
}
