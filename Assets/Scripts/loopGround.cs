using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CameraDatas;

public class loopGround : MonoBehaviour
{
	  public Vector2 movement;
      private Vector2 size;
      private float positionRestartX,positionRestartY;
    // Start is called before the first frame update
    void Start()
    {
        size = gameObject.GetComponent<SpriteRenderer>().bounds.size;
        positionRestartX= GameObject.Find("ground2").GetComponent<Transform>().position.x;
        positionRestartY= GameObject.Find("ground2").GetComponent<Transform>().position.y;

    }

    // Update is called once per frame
    void Update()
    {

       gameObject.GetComponent<Rigidbody2D>().velocity = movement;

       if(  gameObject.GetComponent<Transform>().position.x  < datas.cameraBorderDotLeftBottom.x-(size.x/2)){
            gameObject.GetComponent<Transform>().position = new Vector2(positionRestartX,positionRestartY);
       }


    }
}