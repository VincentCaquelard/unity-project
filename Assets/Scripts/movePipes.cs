﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CameraDatas;

public class movePipes : MonoBehaviour
{

	public Vector2 movement;
	public GameObject pipe1Up;
	public GameObject pipe1Down;
	private Transform pipe1UpOriginalTransform;
	private Transform pipe1DownOriginalTransform;

	private Vector2 siz;
	private float screenCenter;
	private float screenSize;

	private float maxY_upPipe;
	private float minY_upPipe;

	private float maxY_downPipe;
	private float minY_downPipe;
    // Start is called before the first frame update
    void Start()
    {
        siz = pipe1Up.GetComponent<SpriteRenderer>().bounds.size;
        pipe1UpOriginalTransform = pipe1Up.GetComponent<Transform>();
        pipe1DownOriginalTransform = pipe1Down.GetComponent<Transform>();


        float groundSize =  GameObject.Find("ground1").GetComponent<SpriteRenderer>().bounds.size.y;

	    maxY_upPipe = datas.cameraBorderDotRightBottom.y + groundSize;
	    minY_upPipe = (datas.cameraBorderDotRightBottom.y + groundSize) - (siz.y /2) ;

    }

    // Update is called once per frame
    /*void Update()
    {
        pipe1Up.GetComponent<Rigidbody2D>().velocity = movement;
        pipe1Down.GetComponent<Rigidbody2D>().velocity = movement;
        if(  pipe1Up.GetComponent<Transform>().position.x  < datas.cameraBorderDotLeftBottom.x-(size.x/2)){
            pipe1Up.GetComponent<Transform>().position = 
                        new Vector2(datas.cameraBorderDotRightBottom.x,
                        			pipe1UpOriginalTransform.position.y -
            	                    	Random.Range(screenCenter - screenCenter/2,
            	                    				 screenCenter + screenCenter/2));
        }
       if(  pipe1Down.GetComponent<Transform>().position.x  < datas.cameraBorderDotLeftBottom.x-(size.x/2)){
            pipe1Down.GetComponent<Transform>().position = 
                        new Vector2(datas.cameraBorderDotRightBottom.x,
                        			pipe1DownOriginalTransform.position.y -
            	                    	Random.Range(screenCenter - screenCenter/2,
            	                    				 screenCenter + screenCenter/2));
       }
    }*/

    void Update()
	{
		pipe1Up.GetComponent<Rigidbody2D>().velocity = movement; // Déplacement du pipe haut
		pipe1Down.GetComponent<Rigidbody2D>().velocity = movement; // Déplacement du pipe bas
		// Le pipe est sor; de l’écran ? Si oui appel de la méthode moveTORightPipe
		if (pipe1Up.GetComponent<Transform>().position.x < datas.cameraBorderDotLeftBottom.x - (siz.x / 2))
		         moveToRightPipe();
	}

	void moveToRightPipe(){
		float randomY = Random.Range (1,4) - 2; // Tirage aléatoire d’un décalage en Y

		float posX =  datas.cameraBorderDotRightBottom.x + 2*siz.x; // Calcul du X du bord droite de l’écran
		// Calcul du nouvel Y en reprenant la posi;on Y d’origine du pipe, ici le downPipe1
		float newY = pipe1UpOriginalTransform.position.y + randomY;
		// Ce code sert garder les pipes dans l'espace de jeu
		if( newY > maxY_upPipe || newY < minY_upPipe){
			randomY = 0;
		}

		float posY = pipe1UpOriginalTransform.position.y + randomY;
		// Créa;on du vector3 contenant la nouvelle posi;on
		Vector3 tmpPos = new Vector3 (posX, posY, pipe1Up.transform.position.z);
		// Affecta;on de ceVe nouvelle posi;on au transform du gameObject
		pipe1Up.GetComponent<Transform>().position = tmpPos;
		// Idem pour le second pipe
		posY = pipe1DownOriginalTransform.position.y + randomY;


		tmpPos = new Vector3 (posX,posY, pipe1Down.GetComponent<Transform>().position.z);
		pipe1Down.GetComponent<Transform>().position = tmpPos;
	}

  
}
