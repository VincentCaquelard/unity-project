﻿using System.Collections.Generic;
using UnityEngine.UI;
 using UnityEngine;
 using System.Collections;
 


public class SoundState : MonoBehaviour {
    public static SoundState Instance;
    public AudioClip flapSound;
    public AudioClip backgroundMusic;
    public AudioClip clickSound;
    public AudioClip gameOverSound;
    public AudioClip upPointSound;
    private AudioSource audiosourceMusic, audiosourceGameOver,audiosourceFlap,audiosourcePoints,audiosourceButton;


    void Start () {
        if (Instance == null) {
            Instance = this;
            DontDestroyOnLoad (Instance.gameObject);
            init();
        }
        else if (this != Instance) {
            Debug.Log ("Detruit");
            Destroy (this.gameObject);
        }
    }


   public void init(){
    	audiosourceMusic= gameObject.AddComponent<AudioSource>();
    	audiosourceMusic.clip= backgroundMusic;

    	audiosourceGameOver= gameObject.AddComponent<AudioSource>();
    	audiosourceGameOver.clip= gameOverSound;

    	audiosourceButton= gameObject.AddComponent<AudioSource>();
    	audiosourceButton.clip= clickSound;

    	audiosourcePoints= gameObject.AddComponent<AudioSource>();
    	audiosourcePoints.clip= upPointSound;

    	audiosourceFlap= gameObject.AddComponent<AudioSource>();
    	audiosourceFlap.clip= flapSound;
    	
    
    }
    
   public void	makeflapSound(){	
   		audiosourceFlap.Play();
	}	
	public void	makeclickSound(){	
   		audiosourceButton.Play();
	}	
	public void	makeUpPointSound(){	
   		audiosourcePoints.Play();
	}	
	public void	makegameOverSound(){	
   		audiosourceGameOver.Play();
	}	

	 public void startMusic(){	
		audiosourceMusic.Play();
	}	

	public void stopMusic(){
		audiosourceMusic.Stop();
	}
						
///	Play a sound					
	private	void MakeSound(AudioClip originalClip){	
		 Debug.Log ("PlayingSound");
			AudioSource.PlayClipAtPoint(originalClip,transform.position);	
	}	
}
