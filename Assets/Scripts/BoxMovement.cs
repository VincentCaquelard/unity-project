﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CameraDatas;

public class BoxMovement : MonoBehaviour
{
	public Vector2 movement;
	public GameObject pipe;
	private Vector2 sizePipe,sizeBox;
    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<Rigidbody2D>().velocity = movement;
        sizePipe = pipe.GetComponent<SpriteRenderer>().bounds.size;
        sizeBox = gameObject.GetComponent<SpriteRenderer>().bounds.size;
    }

    // Update is called once per frame
    void Update()
    {
    	if (gameObject.GetComponent<Transform>().position.x < datas.cameraBorderDotLeftBottom.x - (sizeBox.x / 2))
        	gameObject.GetComponent<Transform>().position = new Vector2(pipe.GetComponent<Transform>().position.x ,
        	 														 pipe.GetComponent<Transform>().position.y + (sizePipe.y/2) + (sizeBox.y/2));
    }
}
