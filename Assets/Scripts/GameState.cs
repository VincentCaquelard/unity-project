﻿using System.Collections.Generic;
using UnityEngine.UI;
 using UnityEngine;
 using System.Collections;
 


public class GameState : MonoBehaviour {
    public static GameState Instance;
    private int scorePlayer = 0;
    private int level = 0;

    void Start () {
        if (Instance == null) {
            Instance = this;
            DontDestroyOnLoad (Instance.gameObject);
        }
        else if (this != Instance) {
            Debug.Log ("Detruit");
            Destroy (this.gameObject);
        }
    }


  void FixedUpdate(){
        if(GameObject.FindWithTag("scorelabel")!=null)
            GameObject.FindWithTag("scorelabel").GetComponent<Text>().text = scorePlayer.ToString();
       
    }
    
    public void addScorePlayer(int toAdd) {
        scorePlayer += toAdd;
    }

    public int getScorePlayer(){
        return scorePlayer;

    }
    public int getLevel(){
        return level;
    }
    public void changeLevel(){
        if(level ==0)
            level = 1;
        else
            level = 0;
    }
     public void resetScore(){
        scorePlayer = 0;

    }
}
