﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class collideManagementBird : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
    	
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter2D(Collider2D col)
    {
    	//Debug.Log(col.gameObject.name);
    	string name = col.gameObject.name;
    	if(name == "downPipe" || name == "upPipe" || name == "ground1" || name == "ground2"){
    		SoundState.Instance.makegameOverSound();
    		Destroy(GetComponent<touchAction>());
            SoundState.Instance.stopMusic();
    		SceneManager.LoadScene("scene5-GameOver");
    	}
    	if(name =="box1" || name =="box2"){
    		 SoundState.Instance.makeUpPointSound();
    		 GameState.Instance.addScorePlayer(1);
    	}
    }
     void endAction(){

     }
}
