===== Flappy Bird Unity====
=====Caquelard Vincent=====

Compte rendu :

Voici la liste des fonctionnalités qui ont été integrés (en plus de celles contenues dans le TP):

1) Un système de meilleur score.
2) Effet sonore lors du game over, lors du mouvement de l'oiseau et de l'ajout d'un point.
3) Ajout d'une musique 
4) Ajout d'un système de changement de level :
      - Tous les 20 points , le jeu alterne entre la scene3-Game (mode jour avec les pipes droit comme dans le tp) et la scene4-Game_2
      ( mode nuit et pipes incliné pour modifier légèrement le gameplay)

Ce qui n'a pas été intégré :

1) Les items bonus
2) La rotation de l'oiseau
3) Ajout de nuages

Ces fonctionnalités n'ont pas été intégrées principalement par manque de temps.

Difficultés rencontrées :
   - J'ai rencontré plusieurs difficultés lors de l'ajout de son dans le jeu, 
   comme par exemple le son qui était de mauvaise qualité. Ce problème venait 
   du fait qu'un effet sonore pouvait se lançait plusieurs fois en même temps, 
   j'ai pu résoudre cela grâce à une bonne gestion des AudioSources.

   - Je n'ai pas réussi à faire une transition propre entre mes deux niveau de jeu,
    car la position de l'oiseau est réinitialisée à chaque changement de scene. 